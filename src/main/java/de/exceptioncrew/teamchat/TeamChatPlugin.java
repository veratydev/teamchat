package de.exceptioncrew.teamchat;

import com.google.inject.Guice;
import com.google.inject.Injector;
import de.exceptioncrew.teamchat.message.command.MessageCommand;
import de.exceptioncrew.teamchat.message.MessagePostListener;
import de.exceptioncrew.teamchat.message.command.ToggleCommand;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

public final class TeamChatPlugin extends Plugin {
  private static final String TEAM_CHAT_PERMISSION = "staff.chat";

  private Injector injector;

  public TeamChatPlugin() {
    this.injector = Guice.createInjector(InjectModule.create(TEAM_CHAT_PERMISSION));
  }

  @Override
  public void onEnable() {
    PluginManager plugins = getProxy().getPluginManager();
    plugins.registerCommand(this, injector.getInstance(MessageCommand.class));
    plugins.registerCommand(this, injector.getInstance(ToggleCommand.class));
    plugins.registerListener(this, injector.getInstance(MessagePostListener.class));
  }

  @Override
  public void onDisable() {
  }
}
