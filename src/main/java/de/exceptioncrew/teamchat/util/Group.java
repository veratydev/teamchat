package de.exceptioncrew.teamchat.util;

import java.util.stream.Stream;

public interface Group<E> {

  Stream<E> members();

  boolean contains(E entry);
}
