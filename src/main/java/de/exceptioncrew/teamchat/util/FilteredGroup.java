package de.exceptioncrew.teamchat.util;

import com.google.common.base.Preconditions;

import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

public final class FilteredGroup<E> implements Group<E> {
  private Predicate<E> filter;
  private Supplier<Stream<E>> unfiltered;

  private FilteredGroup(Supplier<Stream<E>> unfiltered, Predicate<E> filter) {
    this.filter = filter;
    this.unfiltered = unfiltered;
  }

  @Override
  public Stream<E> members() {
    return unfiltered.get().filter(filter);
  }

  @Override
  public boolean contains(E entry) {
    return filter.test(entry);
  }

  public static <E> FilteredGroup<E> create(Supplier<Stream<E>> unfiltered, Predicate<E> filter) {
    Preconditions.checkNotNull(unfiltered);
    Preconditions.checkNotNull(filter);
    return new FilteredGroup<>(unfiltered, filter);
  }
}
