package de.exceptioncrew.teamchat.util;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Objects;
import java.util.UUID;

public final class RemotePlayer {
  private UUID id;
  private String name;
  private String proxyId;

  private RemotePlayer(UUID id, String name, String proxyId) {
    this.id = id;
    this.name = name;
    this.proxyId = proxyId;
  }

  public UUID getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getProxyId() {
    return proxyId;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
      .add("id", id)
      .add("name", name)
      .add("proxyId", proxyId)
      .toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, proxyId);
  }

  @Override
  public boolean equals(Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof RemotePlayer)) {
      return false;
    }
    RemotePlayer player = (RemotePlayer)object;
    return player.id.equals(id)
      && player.name.equals(name)
      && player.proxyId.equals(proxyId);
  }

  public static RemotePlayer of(ProxiedPlayer player) {
    Preconditions.checkNotNull(player);
    return createLocal(player.getUniqueId(),player.getDisplayName(), ProxyServer.getInstance());
  }

  public static RemotePlayer createLocal(UUID id, String name, ProxyServer server) {
    return create(id, name, server.getName());
  }

  public static RemotePlayer create(UUID id, String name, String proxyId) {
    Preconditions.checkNotNull(id);
    Preconditions.checkNotNull(name);
    return new RemotePlayer(id, name, proxyId);
  }
}
