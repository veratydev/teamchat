package de.exceptioncrew.teamchat.message;

import net.md_5.bungee.api.chat.BaseComponent;

public interface MessageFormat {

  BaseComponent[] format(Message message);
}
