package de.exceptioncrew.teamchat.message;

import de.exceptioncrew.teamchat.util.Group;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import javax.inject.Inject;
public final class MessagePostListener implements Listener  {
  private MessageFormat format;
  private Group<ProxiedPlayer> targetGroup;

  @Inject
  private MessagePostListener(MessageFormat format, Group<ProxiedPlayer> targetGroup) {
    this.format = format;
    this.targetGroup = targetGroup;
  }

  @EventHandler
  public void receiveMessage(MessagePostEvent event) {
    Message message = event.message();
    BaseComponent[] formatted = format.format(message);
    targetGroup.members().forEach(player -> player.sendMessage(formatted));
  }
}
