package de.exceptioncrew.teamchat.message;

import com.google.inject.Inject;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;

public final class SimpleMessageFormat implements MessageFormat {
  private static final class Lazy{
    static MessageFormat INSTANCE = new SimpleMessageFormat();
  }

  @Inject
  private SimpleMessageFormat() {}

  @Override
  public BaseComponent[] format(Message message) {
    String legacy = String.format("§7[§3TeamChat §7- %s] §f%s", message.getSender().getName(), message.getText());
    return TextComponent.fromLegacyText(legacy);
  }

  public static MessageFormat create() {
    return Lazy.INSTANCE;
  }
}
