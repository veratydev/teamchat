package de.exceptioncrew.teamchat.message.command;

import com.google.common.base.Joiner;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import de.exceptioncrew.teamchat.TeamChatPlugin;
import de.exceptioncrew.teamchat.message.Message;
import de.exceptioncrew.teamchat.message.MessageBroadcaster;
import de.exceptioncrew.teamchat.message.StaffMuteTable;
import de.exceptioncrew.teamchat.util.RemotePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public final class MessageCommand extends Command  {
  public static final String NAME = "tc";

  private String permission;
  private MessageBroadcaster broadcaster;
  private StaffMuteTable muteTable;

  @Inject
  private MessageCommand(
      @Named("permission") String permission,
      MessageBroadcaster broadcaster,
      StaffMuteTable muteTable) {

    super(NAME);
    this.permission = permission;
    this.broadcaster = broadcaster;
    this.muteTable = muteTable;
  }

  @Override
  public void execute(CommandSender sender, String[] arguments) {
    if (!(sender instanceof ProxiedPlayer)) {
      sender.sendMessage("This command is not accessible from the console");
      return;
    }

    ProxiedPlayer player = (ProxiedPlayer) sender;
    if (!player.hasPermission(permission)) {
      sender.sendMessage(TextComponent.fromLegacyText("§7[§3TeamChat§7] §cDu hast keine Rechte diesen Befehl zu nutzen."));
      return;
    }

    if (muteTable.get(player.getUniqueId()) != StaffMuteTable.State.ENABLED) {
      sender.sendMessage(TextComponent.fromLegacyText("§7[§3TeamChat§7] §cAktiviere den Teamchat."));
      return;
    }

    String text = Joiner.on(" ").join(arguments);
    Message message = Message.createNow(RemotePlayer.of(player), text);
    broadcaster.broadcast(message);
  }
}
