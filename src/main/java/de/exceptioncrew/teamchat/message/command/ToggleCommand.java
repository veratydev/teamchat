package de.exceptioncrew.teamchat.message.command;

import de.exceptioncrew.teamchat.message.StaffMuteTable;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import javax.inject.Inject;
import javax.inject.Named;

public final class ToggleCommand extends Command {
  private static final String NAME = "tctoggle";

  private String permission;
  private StaffMuteTable table;

  @Inject
  private ToggleCommand(@Named("permission") String permission, StaffMuteTable table) {
    super(NAME);

    this.permission = permission;
    this.table = table;
  }

  @Override
  public void execute(CommandSender sender, String[] args) {
     if (!(sender instanceof ProxiedPlayer)) {
      sender.sendMessage("This command is not accessible from the console");
      return;
    }

    ProxiedPlayer player = (ProxiedPlayer) sender;
    if (!player.hasPermission(permission)) {
      sender.sendMessage(TextComponent.fromLegacyText("§7[§3TeamChat§7] §cDu hast keine Rechte diesen Befehl zu nutzen."));
      return;
    }

    switch (table.toggle(player.getUniqueId())) {
      case ENABLED:
        player.sendMessage(TextComponent.fromLegacyText("§7[§3TeamChat§7] §aDu hast dich in den TeamChat eingeloggt."));
        break;
      case MUTED:
        player.sendMessage(TextComponent.fromLegacyText("§7[§3TeamChat§7] §cDu hast dich aus dem TeamChat ausgeloggt."));
        break;
    }
  }
}
