package de.exceptioncrew.teamchat.message;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import de.exceptioncrew.teamchat.util.RemotePlayer;

import java.util.Objects;

public final class Message {
  private RemotePlayer sender;
  private long timestamp;
  private String text;

  private Message(RemotePlayer sender, long timestamp, String text) {
    this.sender =sender;
    this.timestamp = timestamp;
    this.text =text;
  }

  public RemotePlayer getSender() {
    return sender;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public String getText() {
    return text;
  }

  @Override
  public boolean equals(Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof Message)) {
      return false;
    }
    Message message = (Message) object;
    return message.sender.equals(sender)
      && message.timestamp == timestamp
      && message.text.equals(text);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sender, timestamp, text);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
      .add("sender", sender)
      .add("timestamp", timestamp)
      .add("text", text)
      .toString();
  }

  public static Message createNow(RemotePlayer sender, String text) {
    return create(sender, text, System.currentTimeMillis());
  }

  public static Message create(RemotePlayer sender,String text, long timestamp) {
    Preconditions.checkNotNull(sender);
    Preconditions.checkNotNull(text);
    return new Message(sender, timestamp, text);
  }
}
