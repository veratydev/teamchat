package de.exceptioncrew.teamchat.message;

import com.google.common.base.Preconditions;
import net.md_5.bungee.api.plugin.Event;

public final class MessagePostEvent extends Event {
  private Message message;
  private MessagePostEvent(Message message) {
    this.message = message;
  }

  public Message message() {
    return message;
  }

  public static MessagePostEvent create(Message message) {
    Preconditions.checkNotNull(message);
    return new MessagePostEvent(message);
  }
}
