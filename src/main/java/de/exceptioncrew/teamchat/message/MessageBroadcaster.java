package de.exceptioncrew.teamchat.message;

public interface MessageBroadcaster {

  void broadcast(Message message);
}
