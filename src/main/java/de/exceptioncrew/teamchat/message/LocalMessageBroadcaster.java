package de.exceptioncrew.teamchat.message;

import com.google.common.base.Preconditions;
import com.google.inject.Inject;
import net.md_5.bungee.api.plugin.PluginManager;

public final class LocalMessageBroadcaster implements MessageBroadcaster{
  private PluginManager eventChannel;

  @Inject
  private LocalMessageBroadcaster(PluginManager eventChannel) {
    this.eventChannel = eventChannel;
  }

  @Override
  public void broadcast(Message message) {
    eventChannel.callEvent(MessagePostEvent.create(message));
  }

  public static MessageBroadcaster create(PluginManager eventChannel) {
    Preconditions.checkNotNull(eventChannel);
    return new LocalMessageBroadcaster(eventChannel);
  }
}
