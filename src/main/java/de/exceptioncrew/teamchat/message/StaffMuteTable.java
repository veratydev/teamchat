package de.exceptioncrew.teamchat.message;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class StaffMuteTable {
  public static enum State {
    MUTED,
    ENABLED
  }

  private Set<UUID> disabled;
  private Lock lock;

  private StaffMuteTable(Set<UUID> disabled, Lock lock) {
    this.disabled = disabled;
    this.lock = lock;
  }

  public State toggle(UUID id) {
    lock.lock();
    try {
      if (!disabled.add(id)) {
        disabled.remove(id);
        return State.ENABLED;
      }
      return State.MUTED;
    } finally {
      lock.unlock();
    }
  }

  public State get(UUID uuid) {
    lock.lock();
    try {
      return disabled.contains(uuid) ? State.MUTED : State.ENABLED;
    } finally {
      lock.unlock();
    }
  }

  public static StaffMuteTable create() {
    return new StaffMuteTable(new HashSet<>(), new ReentrantLock());
  }
}
