package de.exceptioncrew.teamchat;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import de.exceptioncrew.teamchat.message.*;
import de.exceptioncrew.teamchat.util.FilteredGroup;
import de.exceptioncrew.teamchat.util.Group;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.PluginManager;

public final class InjectModule extends AbstractModule  {
  private String teamChatPermission;
  private InjectModule(String teamChatPermission) {
    this.teamChatPermission = teamChatPermission;
  }

  @Override
  protected void configure() {
    bind(MessageBroadcaster.class).to(LocalMessageBroadcaster.class).asEagerSingleton();
    bind(MessageFormat.class).to(SimpleMessageFormat.class).asEagerSingleton();
    bind(String.class).annotatedWith(Names.named("permission")).toInstance(teamChatPermission);
  }

  @Provides
  @Singleton
  ProxyServer getServer() {
    return ProxyServer.getInstance();
  }

  @Inject
  @Singleton
  @Provides
  PluginManager getPluginManager(ProxyServer server) {
    return server.getPluginManager();
  }


  @Inject
  @Singleton
  @Provides
  StaffMuteTable createStaffMuteTable() {
    return StaffMuteTable.create();
  }

  @Inject
  @Singleton
  @Provides
  Group<ProxiedPlayer> createStaffGroup(StaffMuteTable table, ProxyServer server) {
    return FilteredGroup.create(() -> server.getPlayers().stream(), (player) -> filterPlayer(player, table));
  }

  private boolean filterPlayer(ProxiedPlayer player, StaffMuteTable table) {
    if (!player.hasPermission(teamChatPermission)){
      return false;
    }
    return table.get(player.getUniqueId()) == StaffMuteTable.State.ENABLED;
  }

  public static InjectModule create(String teamChatPermission) {
    Preconditions.checkNotNull(teamChatPermission);
    return new InjectModule(teamChatPermission);
  }
}
